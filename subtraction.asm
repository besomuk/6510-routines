; Numbers are stored in $2000 ( for no particular reason )
; $2000 - low bit of first number
; $2001 - high bit of fist number
; $2002 - low bit of second number
; $2003 - high bit of second number


; 10 SYS (4096)

*=$0801

        BYTE $0E, $08, $0A, $00, $9E, $20, $28
        BYTE $34, $30, $39, $36, $29, $00, $00, $00

*=$1000

START   JSR SETUP  ; JUMP TO SUBROUTINE
        SEC        ; BEFORE SUBTRACTION, WE MUST SET CARRY !!!
        LDA $2000  ; LOAD LOW BYTE FROM $2000
        SBC $2002  ; ADD VALUE AT $2002 WITH CARRY
        STA $2008  ; PUT RESULT IN $2008

        LDA $2001  ; ADD HIGH BYTE
        SBC $2003
        STA $2009

        BRK

; THIS SETS 2BB9 AND 0AE5 ON MEMORY ADRESES. EACH NUMBER REQUIRES 2 ADDRESSES.

SETUP   LDA #$B9   ; LOW BYTE FOR NUMBER 1
        STA $2000
        LDA #$2B   ; HIGH BYTE FOR NUMBER 1
        STA $2001
        LDA #$E5   ; LOW BYTE FOR NUMBER 2
        STA $2002
        LDA #$0A   ; HIGH BYTE FOR NUMBER 2
        STA $2003
        RTS

        