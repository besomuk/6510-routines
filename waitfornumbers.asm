; 10 SYS (4096)
; This code waits for input from keyboard.
; It waits for 0-9 numbers or RUN/STOP key.
; After it gets numeric, it puts it on the screen and the end.

*=$0801

        BYTE    $0E, $08, $0A, $00, $9E, $20, $28,  $34, $30, $39, $36, $29, $00, $00, $00

*=$1000

START   JSR $FFE1       ; $FFE1 ROUTINE CHECKS IF RUNSTOP IS PRESSED
        BEQ EXIT        ; IF RUN/STOP KEY, GO TO EXIT
        JSR $FFE4       ; $FFE4 GETS CHARACTER FROM KEYBOARD AND PUTS IT'S 8BIT VALUE IN A REGISTER
                        ; IF A = 0, NO KEY IS PRESSED
        CMP #$30        ; LESS THAN 30, NOT NUMBER
        BCC START       ; BCC IS USED FOR CHECKING < OR > VALUES FOR 8 BIT NUMBERS.
        CMP #$3A        ; SAME AS BCC, BUT THIS CHECKS FOR CARRY BIT SET
        BCS START
        JSR $FFD2       ; OUTPUT WHATS IN ACCUMULATOR TO SCREEN
EXIT    RTS

