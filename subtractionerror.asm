*=$0801

        BYTE $0E, $08, $0A, $00, $9E, $20, $28
        BYTE $34, $30, $39, $36, $29, $00, $00, $00

*=$1000

START   JSR SETUP  ; JUMP TO SUBROUTINE
        SEC        ; BEFORE SUBTRACTION, WE MUST SET CARRY !!!

        LDA $2000
        SBC $2014
        STA $2028

        BRK

; THIS SETS 2BB9 AND 0AE5 ON MEMORY ADRESES. EACH NUMBER REQUIRES 2 ADDRESSES.

SETUP   LDA #$01
        STA $2000
        LDA #$02
        STA $2014
        RTS

        