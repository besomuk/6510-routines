; 10 SYS (4096)

*=$0801

        BYTE $0E, $08, $0A, $00, $9E, $20, $28
        BYTE $34, $30, $39, $36, $29, $00, $00, $00

*=$1000

; - VARS --------------------------------------------------;
BORDER = $D020          ; BORDER ADDR
SCREEN = $D021          ; SCREEN ADDR
MSGCH  = $2000

; MAIN
START   JSR SETSCR      ; SET SCREEN
        LDA #$01        ; CHOOSE MESSAGE
        STA MSGCH       ; SET IT TO 2000
        JSR WMSG        ; WRITE MSG
STOP    JMP STOP

; SET SCREEN
SETSCR  JSR $FF81
        LDA #$00
        STA BORDER
        STA SCREEN
        RTS

; WRITE MSG
WMSG    LDX #$00
WMSGL   LDA MSGCH
        CMP #$01
        BEQ M1
        CMP #$02
        BEQ M2
        RTS
M1      LDA MSG1,X
        JMP CONT
M2      LDA MSG2,X
        JMP CONT
CONT    BEQ EXIT
        JSR $FFD2
        INX
        BNE WMSGL
EXIT    RTS

MSG1    TEXT "hello world 1"
        BYTE 0

MSG2    TEXT "hello world 2"
        BYTE 0